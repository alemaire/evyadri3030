Par votre présence et vos attentions, vous avez contribué à la réussite de cette journée inoubliable.

_**Nous vous en remercions de tout coeur !!!**_

Si vous souhaitez encore participer à l'amélioration de notre nid douillet, c'est toujours possible via le compte BE12 0017 8552 7092

---

Désolé de ne pas avoir pu rétablir le site plus tôt.


Explications techniques pour les G33Ks ...
Mon cluster Docker Swarm sur raspberry pi 3 (avec ArchLinuxARM AArch64) n'arrive plus à démarrer correctement suite à des coupure de courrant.

Il a fallu un peu de temps avant d'accepter ce fait et de migrer le site vers GitLab Pages.
Chose désormais faite et code accessible.

https://gitlab.com/alemaire/evyadri3030
